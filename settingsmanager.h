#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QObject>

class SettingsManager : public QObject
{
    Q_OBJECT
public:
    explicit SettingsManager(QObject *parent = nullptr);

signals:

public slots:
    QString GetUsername();
    QString GetUsernameAscii();
    QString GetUsernameColor();
    void SetUsername( const QString &UserName );
    void SetUsernameColor( const QString &color );
    void ToggleTimeStamp();

private:
    QString username_, usernameColorCode_;
    bool timeStamp_;
};

#endif // SETTINGSMANAGER_H
