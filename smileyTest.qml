import QtQuick 2.0

Item {
    anchors.fill: parent
    GridView {
        anchors.fill: parent
        model: smileyListModel
        cellHeight: 30
        cellWidth: 30
        delegate:
            Image {
                height: 30
                width: 30
                fillMode: Image.PreserveAspectFit
                source: path

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log( path )
                    }
                }
            }
    }

    ListModel {
        id: smileyListModel
        ListElement {
            path: "smileys/smiley/afro-1.png" }
        ListElement {
            path: "smileys/smiley/afro.png" }
        ListElement {
            path: "smileys/smiley/agent.png" }
        ListElement {
            path: "smileys/smiley/alien-1.png" }
        ListElement {
            path: "smileys/smiley/alien.png" }
        ListElement {
            path: "smileys/smiley/angel.png" }
        ListElement {
            path: "smileys/smiley/angry-1.png" }
        ListElement {
            path: "smileys/smiley/angry-2.png" }
        ListElement {
            path: "smileys/smiley/angry-3.png" }
        ListElement {
            path: "smileys/smiley/angry-4.png" }
        ListElement {
            path: "smileys/smiley/angry-5.png" }
        ListElement {
            path: "smileys/smiley/angry.png" }
        ListElement {
            path: "smileys/smiley/arguing.png" }
        ListElement {
            path: "smileys/smiley/arrogant.png" }
        ListElement {
            path: "smileys/smiley/asian-1.png" }
        ListElement {
            path: "smileys/smiley/asian.png" }
        ListElement {
            path: "smileys/smiley/avatar.png" }
        ListElement {
            path: "smileys/smiley/baby-1.png" }
        ListElement {
            path: "smileys/smiley/baby-2.png" }
        ListElement {
            path: "smileys/smiley/baby.png" }
        ListElement {
            path: "smileys/smiley/bully.png" }
        ListElement {
            path: "smileys/smiley/burglar.png" }
        ListElement {
            path: "smileys/smiley/businessman.png" }
        ListElement {
            path: "smileys/smiley/cheeky-1.png" }
        ListElement {
            path: "smileys/smiley/cheeky.png" }
        ListElement {
            path: "smileys/smiley/clown.png" }
        ListElement {
            path: "smileys/smiley/confused-1.png" }
        ListElement {
            path: "smileys/smiley/confused-2.png" }
        ListElement {
            path: "smileys/smiley/confused-3.png" }
        ListElement {
            path: "smileys/smiley/confused.png" }
        ListElement {
            path: "smileys/smiley/creepy.png" }
        ListElement {
            path: "smileys/smiley/crying-1.png" }
        ListElement {
            path: "smileys/smiley/crying-2.png" }
        ListElement {
            path: "smileys/smiley/crying-3.png" }
        ListElement {
            path: "smileys/smiley/crying.png" }
        ListElement {
            path: "smileys/smiley/dazed-1.png" }
        ListElement {
            path: "smileys/smiley/dazed-2.png" }
        ListElement {
            path: "smileys/smiley/dazed-3.png" }
        ListElement {
            path: "smileys/smiley/dazed.png" }
        ListElement {
            path: "smileys/smiley/dead-1.png" }
        ListElement {
            path: "smileys/smiley/dead-2.png" }
        ListElement {
            path: "smileys/smiley/dead-3.png" }
        ListElement {
            path: "smileys/smiley/dead-4.png" }
        ListElement {
            path: "smileys/smiley/dead-5.png" }
        ListElement {
            path: "smileys/smiley/dead-6.png" }
        ListElement {
            path: "smileys/smiley/dead.png" }
        ListElement {
            path: "smileys/smiley/desperate-1.png" }
        ListElement {
            path: "smileys/smiley/desperate.png" }
        ListElement {
            path: "smileys/smiley/detective.png" }
        ListElement {
            path: "smileys/smiley/dissapointment.png" }
        ListElement {
            path: "smileys/smiley/doctor.png" }
        ListElement {
            path: "smileys/smiley/drunk.png" }
        ListElement {
            path: "smileys/smiley/dumb.png" }
        ListElement {
            path: "smileys/smiley/emo-1.png" }
        ListElement {
            path: "smileys/smiley/emo-2.png" }
        ListElement {
            path: "smileys/smiley/emo.png" }
        ListElement {
            path: "smileys/smiley/emoticon.png" }
        ListElement {
            path: "smileys/smiley/evil.png" }
        ListElement {
            path: "smileys/smiley/faint-1.png" }
        ListElement {
            path: "smileys/smiley/faint.png" }
        ListElement {
            path: "smileys/smiley/flirt-1.png" }
        ListElement {
            path: "smileys/smiley/flirt-2.png" }
        ListElement {
            path: "smileys/smiley/flirt.png" }
        ListElement {
            path: "smileys/smiley/flirty.png" }
        ListElement {
            path: "smileys/smiley/gangster.png" }
        ListElement {
            path: "smileys/smiley/geek-1.png" }
        ListElement {
            path: "smileys/smiley/geek.png" }
        ListElement {
            path: "smileys/smiley/gentleman-1.png" }
        ListElement {
            path: "smileys/smiley/gentleman-2.png" }
        ListElement {
            path: "smileys/smiley/gentleman-3.png" }
        ListElement {
            path: "smileys/smiley/gentleman-4.png" }
        ListElement {
            path: "smileys/smiley/gentleman.png" }
        ListElement {
            path: "smileys/smiley/ginger.png" }
        ListElement {
            path: "smileys/smiley/girl-1.png" }
        ListElement {
            path: "smileys/smiley/girl.png" }
        ListElement {
            path: "smileys/smiley/goofy-1.png" }
        ListElement {
            path: "smileys/smiley/goofy-2.png" }
        ListElement {
            path: "smileys/smiley/goofy-3.png" }
        ListElement {
            path: "smileys/smiley/goofy-4.png" }
        ListElement {
            path: "smileys/smiley/goofy.png" }
        ListElement {
            path: "smileys/smiley/grubby-1.png" }
        ListElement {
            path: "smileys/smiley/grubby.png" }
        ListElement {
            path: "smileys/smiley/happy-1.png" }
        ListElement {
            path: "smileys/smiley/happy-10.png" }
        ListElement {
            path: "smileys/smiley/happy-11.png" }
        ListElement {
            path: "smileys/smiley/happy-12.png" }
        ListElement {
            path: "smileys/smiley/happy-13.png" }
        ListElement {
            path: "smileys/smiley/happy-14.png" }
        ListElement {
            path: "smileys/smiley/happy-15.png" }
        ListElement {
            path: "smileys/smiley/happy-16.png" }
        ListElement {
            path: "smileys/smiley/happy-2.png" }
        ListElement {
            path: "smileys/smiley/happy-3.png" }
        ListElement {
            path: "smileys/smiley/happy-4.png" }
        ListElement {
            path: "smileys/smiley/happy-5.png" }
        ListElement {
            path: "smileys/smiley/happy-6.png" }
        ListElement {
            path: "smileys/smiley/happy-7.png" }
        ListElement {
            path: "smileys/smiley/happy-8.png" }
        ListElement {
            path: "smileys/smiley/happy-9.png" }
        ListElement {
            path: "smileys/smiley/happy.png" }
        ListElement {
            path: "smileys/smiley/harry-potter.png" }
        ListElement {
            path: "smileys/smiley/heisenberg.png" }
        ListElement {
            path: "smileys/smiley/hipster-1.png" }
        ListElement {
            path: "smileys/smiley/hipster-2.png" }
        ListElement {
            path: "smileys/smiley/hipster.png" }
        ListElement {
            path: "smileys/smiley/in-love-1.png" }
        ListElement {
            path: "smileys/smiley/in-love-2.png" }
        ListElement {
            path: "smileys/smiley/in-love-3.png" }
        ListElement {
            path: "smileys/smiley/in-love-4.png" }
        ListElement {
            path: "smileys/smiley/in-love-5.png" }
        ListElement {
            path: "smileys/smiley/in-love-6.png" }
        ListElement {
            path: "smileys/smiley/in-love.png" }
        ListElement {
            path: "smileys/smiley/japan.png" }
        ListElement {
            path: "smileys/smiley/jew.png" }
        ListElement {
            path: "smileys/smiley/joyful-1.png" }
        ListElement {
            path: "smileys/smiley/joyful-2.png" }
        ListElement {
            path: "smileys/smiley/joyful.png" }
        ListElement {
            path: "smileys/smiley/kiss-1.png" }
        ListElement {
            path: "smileys/smiley/kiss-2.png" }
        ListElement {
            path: "smileys/smiley/kiss-3.png" }
        ListElement {
            path: "smileys/smiley/kiss-4.png" }
        ListElement {
            path: "smileys/smiley/kiss.png" }
        ListElement {
            path: "smileys/smiley/laughing-1.png" }
        ListElement {
            path: "smileys/smiley/laughing-2.png" }
        ListElement {
            path: "smileys/smiley/laughing-3.png" }
        ListElement {
            path: "smileys/smiley/laughing.png" }
        ListElement {
            path: "smileys/smiley/listening.png" }
        ListElement {
            path: "smileys/smiley/love.png" }
        ListElement {
            path: "smileys/smiley/manly.png" }
        ListElement {
            path: "smileys/smiley/miserly-1.png" }
        ListElement {
            path: "smileys/smiley/miserly.png" }
        ListElement {
            path: "smileys/smiley/nerd-1.png" }
        ListElement {
            path: "smileys/smiley/nerd-2.png" }
        ListElement {
            path: "smileys/smiley/nerd-3.png" }
        ListElement {
            path: "smileys/smiley/nerd-4.png" }
        ListElement {
            path: "smileys/smiley/nerd.png" }
        ListElement {
            path: "smileys/smiley/ninja.png" }
        ListElement {
            path: "smileys/smiley/pirate-1.png" }
        ListElement {
            path: "smileys/smiley/pirate-2.png" }
        ListElement {
            path: "smileys/smiley/pirate.png" }
        ListElement {
            path: "smileys/smiley/punk-1.png" }
        ListElement {
            path: "smileys/smiley/punk-2.png" }
        ListElement {
            path: "smileys/smiley/punk.png" }
        ListElement {
            path: "smileys/smiley/rapper.png" }
        ListElement {
            path: "smileys/smiley/relieved.png" }
        ListElement {
            path: "smileys/smiley/rich-1.png" }
        ListElement {
            path: "smileys/smiley/rich-2.png" }
        ListElement {
            path: "smileys/smiley/rich.png" }
        ListElement {
            path: "smileys/smiley/rockstar.png" }
        ListElement {
            path: "smileys/smiley/sad-1.png" }
        ListElement {
            path: "smileys/smiley/sad-2.png" }
        ListElement {
            path: "smileys/smiley/sad-3.png" }
        ListElement {
            path: "smileys/smiley/sad-4.png" }
        ListElement {
            path: "smileys/smiley/sad-5.png" }
        ListElement {
            path: "smileys/smiley/sad-6.png" }
        ListElement {
            path: "smileys/smiley/sad.png" }
        ListElement {
            path: "smileys/smiley/scared-1.png" }
        ListElement {
            path: "smileys/smiley/scared-2.png" }
        ListElement {
            path: "smileys/smiley/scared-3.png" }
        ListElement {
            path: "smileys/smiley/scared.png" }
        ListElement {
            path: "smileys/smiley/sceptic-1.png" }
        ListElement {
            path: "smileys/smiley/sceptic-2.png" }
        ListElement {
            path: "smileys/smiley/sceptic-3.png" }
        ListElement {
            path: "smileys/smiley/sceptic-4.png" }
        ListElement {
            path: "smileys/smiley/sceptic-5.png" }
        ListElement {
            path: "smileys/smiley/sceptic-6.png" }
        ListElement {
            path: "smileys/smiley/sceptic-7.png" }
        ListElement {
            path: "smileys/smiley/sceptic.png" }
        ListElement {
            path: "smileys/smiley/secret.png" }
        ListElement {
            path: "smileys/smiley/shocked-1.png" }
        ListElement {
            path: "smileys/smiley/shocked-2.png" }
        ListElement {
            path: "smileys/smiley/shocked-3.png" }
        ListElement {
            path: "smileys/smiley/shocked.png" }
        ListElement {
            path: "smileys/smiley/sick-1.png" }
        ListElement {
            path: "smileys/smiley/sick-2.png" }
        ListElement {
            path: "smileys/smiley/sick-3.png" }
        ListElement {
            path: "smileys/smiley/sick-4.png" }
        ListElement {
            path: "smileys/smiley/sick.png" }
        ListElement {
            path: "smileys/smiley/silent.png" }
        ListElement {
            path: "smileys/smiley/skeleton.png" }
        ListElement {
            path: "smileys/smiley/smile.png" }
        ListElement {
            path: "smileys/smiley/smiling-1.png" }
        ListElement {
            path: "smileys/smiley/smiling.png" }
        ListElement {
            path: "smileys/smiley/smoked.png" }
        ListElement {
            path: "smileys/smiley/smug-1.png" }
        ListElement {
            path: "smileys/smiley/smug-2.png" }
        ListElement {
            path: "smileys/smiley/smug-3.png" }
        ListElement {
            path: "smileys/smiley/smug-4.png" }
        ListElement {
            path: "smileys/smiley/smug-5.png" }
        ListElement {
            path: "smileys/smiley/smug-6.png" }
        ListElement {
            path: "smileys/smiley/smug.png" }
        ListElement {
            path: "smileys/smiley/sporty.png" }
        ListElement {
            path: "smileys/smiley/stunned.png" }
        ListElement {
            path: "smileys/smiley/superhero-1.png" }
        ListElement {
            path: "smileys/smiley/superhero-2.png" }
        ListElement {
            path: "smileys/smiley/superhero-3.png" }
        ListElement {
            path: "smileys/smiley/superhero-4.png" }
        ListElement {
            path: "smileys/smiley/superhero.png" }
        ListElement {
            path: "smileys/smiley/surprised-1.png" }
        ListElement {
            path: "smileys/smiley/surprised.png" }
        ListElement {
            path: "smileys/smiley/thinking.png" }
        ListElement {
            path: "smileys/smiley/tired-1.png" }
        ListElement {
            path: "smileys/smiley/tired-2.png" }
        ListElement {
            path: "smileys/smiley/tired-3.png" }
        ListElement {
            path: "smileys/smiley/tired.png" }
        ListElement {
            path: "smileys/smiley/tough-1.png" }
        ListElement {
            path: "smileys/smiley/tough.png" }
        ListElement {
            path: "smileys/smiley/trendy.png" }
        ListElement {
            path: "smileys/smiley/vampire-1.png" }
        ListElement {
            path: "smileys/smiley/vampire.png" }
        ListElement {
            path: "smileys/smiley/wink-1.png" }
        ListElement {
            path: "smileys/smiley/wink-2.png" }
        ListElement {
            path: "smileys/smiley/wink.png" }
        ListElement {
            path: "smileys/smiley/winking-1.png" }
        ListElement {
            path: "smileys/smiley/winking.png" }
        ListElement {
            path: "smileys/smiley/yawning-1.png" }
        ListElement {
            path: "smileys/smiley/yawning-2.png" }
        ListElement {
            path: "smileys/smiley/yawning-3.png" }
        ListElement {
            path: "smileys/smiley/yawning.png" }
        ListElement {
            path: "smileys/smiley/yelling.png" }
        ListElement {
            path: "smileys/smiley/zombie.png" }
    }
}
