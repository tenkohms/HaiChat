#ifndef CHATPAGEWRAPPER_H
#define CHATPAGEWRAPPER_H

#include <QObject>

class ChatPageWrapper : public QObject
{
    Q_OBJECT
public:
    explicit ChatPageWrapper(QObject *parent = nullptr);

    void setUsers( QStringList Users );
signals:
    void newTextLine( const QString &message );
    void usersChanged( QStringList Users );

public slots:
    QString Users();
    void AppendText( const QString &message );
    void ReceiveCommand( const QString &command );

    void openURL( const QString &url );

private:
    QStringList users_;
};

#endif // CHATPAGEWRAPPER_H
