#include "eventfilters.h"
#include <QDebug>
#include <QApplicationStateChangeEvent>

EventFilters::EventFilters(QObject *parent) : QObject(parent)
{
}

bool EventFilters::eventFilter(QObject *obj, QEvent *event)
{
    if ( event->type() == QEvent::ApplicationActivated )
    {
        emit windowRegainActive();
    }

    return QObject::eventFilter( obj, event );
}
