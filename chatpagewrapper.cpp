#include "chatpagewrapper.h"
#include <QDebug>

#include <QDesktopServices>
#include <QUrl>

ChatPageWrapper::ChatPageWrapper(QObject *parent)
    : QObject(parent),
      users_( QStringList() )
{

}

void ChatPageWrapper::AppendText(const QString &message)
{
    emit newTextLine( message );
}

QString ChatPageWrapper::Users()
{
    QString dUsers;
    foreach ( const QString &mUser, users_ )
        dUsers.append( mUser + "\n" );
    dUsers = dUsers.mid( 0, dUsers.size() - 1 );
    return dUsers;
}

void ChatPageWrapper::setUsers(QStringList Users)
{
    if ( users_ != Users )
    {
        users_ = Users;
        emit usersChanged( users_ );
    }
}

void ChatPageWrapper::ReceiveCommand(const QString &command)
{;
    if ( command.contains( "roomList," ) )
    {
        QString roomList = command;
        roomList = roomList.remove("roomList,");
        QStringList theList = roomList.split(QRegExp(","));
        setUsers(theList);
    }
}

void ChatPageWrapper::openURL(const QString &url)
{
    QDesktopServices::openUrl(QUrl( url ));
}
