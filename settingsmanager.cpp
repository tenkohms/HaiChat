#include "settingsmanager.h"
#include <QDebug>
#include <QTime>

SettingsManager::SettingsManager(QObject *parent)
    : QObject(parent),
      username_( "DefaultUser" ),
      usernameColorCode_( "black" ),
      timeStamp_( false )
{

}

QString SettingsManager::GetUsername()
{
    //qDebug() << "returning: " << QString( "<b><font color=\"" + usernameColorCode_ + "\">" + username_ + " :</b></font>" );

    QString retString = QString( "<b><font color=\"" + usernameColorCode_ + "\">" + username_);
    if ( timeStamp_ ) {
        QTime nowTime;
        retString.append( nowTime.currentTime().toString(" (hh:mm:ss)") );
    }
    retString.append( " : </b></font>" );
    return retString;
}

QString SettingsManager::GetUsernameAscii()
{
    return username_;
}

QString SettingsManager::GetUsernameColor()
{
    return usernameColorCode_;
}

void SettingsManager::SetUsername(const QString &UserName )
{
    username_ = UserName;
}

void SettingsManager::SetUsernameColor(const QString &color)
{
    usernameColorCode_ = color;
}

void SettingsManager::ToggleTimeStamp()
{
    timeStamp_ = !timeStamp_;
}
