#include "websockethandler.h"

#include <QDebug>

WebSocketHandler::WebSocketHandler(QObject *parent)
    : QObject(parent)
    , isConnected_( false )

{
    webSocket_ = new QWebSocket;

    connect( webSocket_, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect( webSocket_, SIGNAL(textMessageReceived(QString)), this, SLOT(onNewTextMessage(QString)));
    connect( webSocket_, SIGNAL(binaryMessageReceived(QByteArray)), this, SLOT(onCommandReceived(QByteArray)));
    connect( webSocket_, SIGNAL(connected()), this, SLOT(onConnected()));

}

WebSocketHandler::~WebSocketHandler()
{
    webSocket_->disconnect();
    webSocket_->deleteLater();
}

void WebSocketHandler::BeginConnection(const QString &address)
{
    if ( !isConnected_ )
    {
        webSocket_->open( address );
    }
}

void WebSocketHandler::SendTextMessage(const QString &message)
{
    if ( isConnected_ )
    {
        webSocket_->sendTextMessage( message );
    }
}

void WebSocketHandler::SendCommand(const QString &message)
{
    if ( isConnected_ )
    {
        webSocket_->sendBinaryMessage( message.toLocal8Bit() );
    }
}

void WebSocketHandler::onConnected()
{
    isConnected_ = true;
    emit connected();
}

void WebSocketHandler::onDisconnected()
{
    isConnected_ = false;
    emit disconnected();
}

void WebSocketHandler::onNewTextMessage(const QString &message)
{
    emit receivedNewMessage( message );
}

void WebSocketHandler::onCommandReceived(const QByteArray &command)
{
    qDebug() << "Command received: " << QString::fromLocal8Bit( command );
    emit commandReceived( QString::fromLocal8Bit(command));
}
