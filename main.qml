import QtQuick 2.8
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtWinExtras 1.0

import ProjectSettings 1.0
import WebSocketHandler 1.0

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 350
    height: 350
    title: qsTr("HaiChat")
    property string serveraddress : "ws://ec2-18-217-162-55.us-east-2.compute.amazonaws.com:9090"
    //property string serveraddress : "ws://127.0.0.1:9090"
    property bool notificationIcon: false

    SettingsManager {
        id: settingsManager
    }

    SocketHandler{
        id: sockethandler
        onConnected: {
            mainLoader.source = "ChatPage.qml"
            disconnectedBanner.visible = false
            sockethandler.SendCommand( "username," + settingsManager.GetUsernameAscii() )
        }
        onDisconnected: {
            disconnectedBanner.visible = true
            mainLoader.source = "UsernamePage.qml"
        }
    }

    Loader {
        id: mainLoader
        anchors.fill: parent
        source: "UsernamePage.qml"
    }

    Rectangle {
        id: disconnectedBanner
        visible: false
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height / 6
        color: "red"
        Text {
            anchors.centerIn: parent
            height: parent.height
            width: parent.width
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
            color: "white"
            text: "Disconnected..."
        }
    }

    TaskbarButton {
        id: taskbarButton
        overlay.iconSource: ""
    }

    function receivedMessageNotification() {
        if ( !mainWindow.active ){
            mainWindow.alert( 3000 )
            notificationIcon = true
            taskbarButton.overlay.iconSource = "/images/messages.png"

        }
    }

    function checkNotifications() {
        if ( notificationIcon )
        {
            notificationIcon = false
            taskbarButton.overlay.iconSource = ""
        }
    }
}
