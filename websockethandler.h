#ifndef WEBSOCKETIMEPLENTATION_H
#define WEBSOCKETIMEPLENTATION_H

#include <QObject>
#include <QWebSocket>

class WebSocketHandler : public QObject
{
    Q_OBJECT
public:
    explicit WebSocketHandler(QObject *parent = nullptr);
    ~WebSocketHandler();


signals:
    void connected();
    void disconnected();
    void receivedNewMessage( const QString &message );
    void commandReceived( const QString &message );


public slots:
    void BeginConnection( const QString &address );
    void SendTextMessage( const QString &message );
    void SendCommand( const QString &message );

    void onNewTextMessage( const QString &message );
    void onCommandReceived( const QByteArray &command);
    void onConnected();
    void onDisconnected();

private:
    QWebSocket * webSocket_;
    bool isConnected_;
};

#endif // WEBSOCKETIMEPLENTATION_H
