#ifndef EVENTFILTERS_H
#define EVENTFILTERS_H

#include <QObject>
#include <QEvent>

class EventFilters : public QObject
{
    Q_OBJECT
public:
    explicit EventFilters(QObject *parent = nullptr);

signals:
    void windowRegainActive();

protected:
    bool eventFilter( QObject * obj, QEvent * event );

public slots:
};

#endif // EVENTFILTERS_H
