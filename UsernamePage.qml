import QtQuick 2.0
import QtQuick.Controls 2.2
Item {
    anchors.fill: parent

    Text {
        id: usernameErrorText
        anchors.bottom: usernameTextField.top
        anchors.bottomMargin: 5
        anchors.horizontalCenter: usernameTextField.horizontalCenter
        color: "red"
        text: "Please enter a username."
        visible: false

    }

    TextField {
        id: usernameTextField
        anchors.centerIn: parent
        width: parent.width * .75
        placeholderText: "Username"
        horizontalAlignment: TextInput.AlignHCenter
    }

    Button {
        id: connectButton
        width: usernameTextField.width * .75
        anchors.top: usernameTextField.bottom
        anchors.topMargin: 5
        anchors.horizontalCenter: usernameTextField.horizontalCenter
        text: "Connect"
        onClicked: {
            if ( usernameTextField.text != "" ) {
                connectButton.enabled = false
                settingsManager.SetUsername( usernameTextField.text )
                sockethandler.BeginConnection( serveraddress )
            }
            else
            {
                usernameErrorText.visible = true
            }
        }
    }
}
