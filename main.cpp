#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "qqml.h"
#include <QWindow>

#include "eventfilters.h"

#include "settingsmanager.h"
#include "websockethandler.h"
#include "chatpagewrapper.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    EventFilters mFilter;

    qmlRegisterType< WebSocketHandler >( "WebSocketHandler", 1, 0, "SocketHandler" );
    qmlRegisterType< SettingsManager >( "ProjectSettings", 1, 0, "SettingsManager" );
    qmlRegisterType< ChatPageWrapper >( "ChatPageWrapper", 1, 0, "ChatPage" );

    QQmlApplicationEngine engine;
    app.installEventFilter( &mFilter );
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QObject::connect( &mFilter, SIGNAL( windowRegainActive()), engine.rootObjects().takeFirst(), SLOT( checkNotifications() ));

    return app.exec();
}
