import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3

import ChatPageWrapper 1.0

Item {
    anchors.fill: parent
    id: cp

    function sendSmiley( smiley )
    {
        sockethandler.SendTextMessage( settingsManager.GetUsername() + "<img source=\"" + smiley + "\" height=\"64\" width=\"64\">" )
    }

    ChatPage {
        id: chatPage
        onNewTextLine: {
            mainTextArea.append( message )
        }
    }

    Connections {
        target: sockethandler
        onReceivedNewMessage: {
            mainWindow.receivedMessageNotification();
            mainTextArea.appendText( message )
        }
        onCommandReceived: {
            chatPage.ReceiveCommand( message )
        }
    }

    Connections {
        target: linkDialog
        onLinkToAppend: {
            bg.appendLink( link )
        }
    }

    ColorDialog {
        id: colorDialog
        title: "Please choose a color"
        visible: false
        onAccepted: {
            settingsManager.SetUsernameColor( colorDialog.color )
            visible = false
        }
        onRejected: {
            visible = false
        }
    }

    Rectangle {
        id: bg
        anchors.fill: parent

        Button {
            id: sendPushButton
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.bottomMargin: 5
            anchors.rightMargin: 5

            text: "Send"
            onClicked: {
                bg.sendMessage()
            }
        }

        TextField {
            id: messageTextEdit
            height: sendPushButton.height
            anchors.right: sendPushButton.left
            anchors.rightMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5

            onAccepted: {
                bg.sendMessage()
            }

        }

        ScrollView {
            id: mainTextArea
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: messageTextEdit.top
            TextArea {
                width: parent.width
                id: ta
                textFormat: TextEdit.AutoText
                wrapMode: TextEdit.Wrap
                readOnly: true
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.NoButton
                    cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                }

                onLinkActivated: {
                    chatPage.openURL( link )
                }

                onTextChanged: {
                    cursorPosition = text.length
                }
            }
            function appendText( text )
            {
                ta.append( text )
            }
            function clearText()
            {
                ta.text = ""
            }
        }

        function appendLink( link )
        {
            messageTextEdit.text = messageTextEdit.text + link
        }



        function sendMessage() {
            if ( messageTextEdit.text != "" ) {

                if ( messageTextEdit.text === "/color" ) {
                    messageTextEdit.text = "";
                    colorDialog.setColor( settingsManager.GetUsernameColor() )
                    colorDialog.visible = true
                }
                else if( messageTextEdit.text === "/clear" ) {
                    messageTextEdit.text = ""
                    mainTextArea.clearText()
                }
                else if( messageTextEdit.text === "/timeStamp" ) {
                    messageTextEdit.text = "";
                    settingsManager.ToggleTimeStamp()
                }
                else if ( messageTextEdit.text === "/room" ) {
                    messageTextEdit.text = "";
                    mainTextArea.appendText( "People in lobby:\n" + chatPage.Users() )
                }
                else if ( messageTextEdit.text === "/link" ) {
                    messageTextEdit.text = "";
                    linkDialog.visible = true
                }
                else if ( messageTextEdit.text === "/smiley" ) {
                    messageTextEdit.text = "";
                    smileyDialog.visible = true
                }
                else if ( messageTextEdit.text === "/help" ) {
                    messageTextEdit.text = "";

                    mainTextArea.appendText( "<u>--- Commands ---</u>" )
                    mainTextArea.appendText( "/help - shows this menu" )
                    mainTextArea.appendText( "/color - set username color" )
                    mainTextArea.appendText( "/timeStamp - toggle timestamps on/off" )
                    mainTextArea.appendText( "/room - show people currently in this chat")
                    mainTextArea.appendText( "/link - helps create links" )
                    mainTextArea.appendText( "/smiley - shows smileys" )
                }


                else
                {
                    sockethandler.SendTextMessage( settingsManager.GetUsername() + messageTextEdit.text )
                    messageTextEdit.text = "";
                }
            }
        }
    } //bg

    Rectangle {
        function smileyDialogSendSmiley( smiley )
        {
            cp.sendSmiley( smiley )
            smileyGrid.positionViewAtBeginning()
            smileyDialog.visible = false
        }

        id: smileyDialog
        visible: false
        anchors.fill: parent
        color: "white"

        Rectangle {
            id: closeBar
            anchors.top: parent.top
            anchors.left: parent.left
            height: parent.height / 12
            width: parent.width
            color: "light grey"

            Button {
                height: parent.height
                width: parent.width / 3
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                text: "Close"
                onClicked: {
                    smileyGrid.positionViewAtBeginning()
                    smileyDialog.visible = false
                }
            }
        }

        GridView {
            id: smileyGrid
            function sendSmiley( smiley )
            {
                smileyDialog.smileyDialogSendSmiley( smiley )
            }

            anchors.top: closeBar.bottom
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width: parent.width
            model: smileyListModel
            cellHeight: { ( big ? 128 : 70 ) }
            cellWidth: { ( big ? 128 : 70 ) }
            clip: true
            delegate:
                Image {
                    height: { ( big ? 128 : 64 ) }
                    width: { ( big ? 128 : 64 ) }
                    fillMode: Image.PreserveAspectFit
                    source: path

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            smileyGrid.sendSmiley( path );
                        }
                    }
                }
        }

        ListModel {
            id: smileyListModel

            ListElement {
                path:"smileys/smiley/001-embarrassed-4.png"; big: false }
            ListElement {
                path:"smileys/smiley/002-sad-14.png"; big: false }
            ListElement {
                path:"smileys/smiley/003-rich-1.png"; big: false }
            ListElement {
                path:"smileys/smiley/004-surprised-1.png"; big: false }
            ListElement {
                path:"smileys/smiley/005-vomit.png"; big: false }
            ListElement {
                path:"smileys/smiley/006-laughing-3.png"; big: false }
            ListElement {
                path:"smileys/smiley/007-angry-4.png"; big: false }
            ListElement {
                path:"smileys/smiley/008-silent.png"; big: false }
            ListElement {
                path:"smileys/smiley/009-surprised.png";  big: false }
            ListElement {
                path:"smileys/smiley/010-favorite.png";  big: false }
            ListElement {
                path:"smileys/smiley/011-suspicious.png";  big: false }
            ListElement {
                path:"smileys/smiley/012-sick-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/013-liar.png";  big: false }
            ListElement {
                path:"smileys/smiley/014-dribble.png";  big: false }
            ListElement {
                path:"smileys/smiley/015-laughing-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/016-sick-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/017-clown.png";  big: false }
            ListElement {
                path:"smileys/smiley/018-cowboy.png";  big: false }
            ListElement {
                path:"smileys/smiley/019-happy-7.png";  big: false }
            ListElement {
                path:"smileys/smiley/020-robot.png";  big: false }
            ListElement {
                path:"smileys/smiley/021-injured.png";  big: false }
            ListElement {
                path:"smileys/smiley/022-thinking-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/023-nerd.png";  big: false }
            ListElement {
                path:"smileys/smiley/024-sick-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/025-rich.png";  big: false }
            ListElement {
                path:"smileys/smiley/026-secret.png";  big: false }
            ListElement {
                path:"smileys/smiley/027-monkey-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/028-monkey-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/029-monkey.png";  big: false }
            ListElement {
                path:"smileys/smiley/030-thinking.png";  big: false }
            ListElement {
                path:"smileys/smiley/031-happy-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/032-happy-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/033-sad-13.png";  big: false }
            ListElement {
                path:"smileys/smiley/034-cat-8.png";  big: false }
            ListElement {
                path:"smileys/smiley/035-cat-7.png";  big: false }
            ListElement {
                path:"smileys/smiley/036-cat-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/037-cat-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/038-cat-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/039-cat-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/040-cat-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/041-cat-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/042-cat.png";  big: false }
            ListElement {
                path:"smileys/smiley/043-sick.png";  big: false }
            ListElement {
                path:"smileys/smiley/044-muted.png";  big: false }
            ListElement {
                path:"smileys/smiley/045-shocked-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/046-sleeping-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/047-embarrassed-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/048-shocked-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/049-shocked-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/050-embarrassed-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/051-shocked-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/052-shocked-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/053-crying-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/054-shocked-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/055-sad-12.png";  big: false }
            ListElement {
                path:"smileys/smiley/056-sad-11.png";  big: false }
            ListElement {
                path:"smileys/smiley/057-sad-10.png";  big: false }
            ListElement {
                path:"smileys/smiley/058-sad-9.png";  big: false }
            ListElement {
                path:"smileys/smiley/059-sad-8.png";  big: false }
            ListElement {
                path:"smileys/smiley/060-sad-7.png";  big: false }
            ListElement {
                path:"smileys/smiley/061-sad-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/062-angry-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/063-sad-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/064-crying-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/065-angry-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/066-angry-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/067-sad-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/068-sad-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/069-tongue-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/070-tongue-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/071-tongue-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/072-kiss-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/073-kiss-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/074-kiss-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/075-kiss.png";  big: false }
            ListElement {
                path:"smileys/smiley/076-sad-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/077-sad-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/078-sad.png";  big: false }
            ListElement {
                path:"smileys/smiley/079-embarrassed-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/080-angry.png";  big: false }
            ListElement {
                path:"smileys/smiley/081-sleeping.png";  big: false }
            ListElement {
                path:"smileys/smiley/082-shocked.png";  big: false }
            ListElement {
                path:"smileys/smiley/083-smart.png";  big: false }
            ListElement {
                path:"smileys/smiley/084-cool.png";  big: false }
            ListElement {
                path:"smileys/smiley/085-in-love.png";  big: false }
            ListElement {
                path:"smileys/smiley/086-happy-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/087-tongue.png";  big: false }
            ListElement {
                path:"smileys/smiley/088-embarrassed.png";  big: false }
            ListElement {
                path:"smileys/smiley/089-wink.png";  big: false }
            ListElement {
                path:"smileys/smiley/090-devil-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/091-angel.png";  big: false }
            ListElement {
                path:"smileys/smiley/092-laughing-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/093-laughing.png";  big: false }
            ListElement {
                path:"smileys/smiley/094-happy-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/095-happy-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/096-crying.png";  big: false }
            ListElement {
                path:"smileys/smiley/097-happy-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/098-happy.png";  big: false }
            ListElement {
                path:"smileys/smiley/099-poo.png";  big: false }
            ListElement {
                path:"smileys/smiley/100-skull.png";  big: false }
            ListElement {
                path:"smileys/smiley/101-devil.png";  big: false }
            ListElement {
                path:"smileys/smiley/102-space-invaders.png";  big: false }
            ListElement {
                path:"smileys/smiley/103-alien.png";  big: false }
            ListElement {
                path:"smileys/smiley/104-ghost.png";  big: false }
            ListElement {
                path:"smileys/smiley/105-demon-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/106-demon.png";  big: false }
            ListElement {
                path:"smileys/smiley/alone.png";  big: true }
            ListElement {
                path:"smileys/smiley/bae.png";  big: true }
            ListElement {
                path:"smileys/smiley/coffee.png";  big: true }
            ListElement {
                path:"smileys/smiley/deepLike.png";  big: true }
            ListElement {
                path:"smileys/smiley/dinner.png";  big: true }
            ListElement {
                path:"smileys/smiley/eyesClosed.png";  big: true }
            ListElement {
                path:"smileys/smiley/ghosting.png";  big: true }
            ListElement {
                path:"smileys/smiley/hanging.png";  big: true }
            ListElement {
                path:"smileys/smiley/hug.png";  big: true }
            ListElement {
                path:"smileys/smiley/kissing.png";  big: true }
            ListElement {
                path:"smileys/smiley/leaveHanging.png";  big: true }
            ListElement {
                path:"smileys/smiley/newPhone.png";  big: true }
            ListElement {
                path:"smileys/smiley/noReply.png";  big: true }
            ListElement {
                path:"smileys/smiley/noStrings.png";  big: true }
            ListElement {
                path:"smileys/smiley/notIntoYou.png";  big: true }
            ListElement {
                path:"smileys/smiley/peachGlasses.png";  big: true }
            ListElement {
                path:"smileys/smiley/peachMartini.png";  big: true }
            ListElement {
                path:"smileys/smiley/player.png";  big: true }
            ListElement {
                path:"smileys/smiley/shy.png";  big: true }
            ListElement {
                path:"smileys/smiley/theOne.png";  big: true }
            ListElement {
                path:"smileys/smiley/thirsty.png";  big: true }
            /*
            ListElement {
                path:"smileys/smiley/001-embarrassed-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/002-sad-14.png";  big: false }
            ListElement {
                path:"smileys/smiley/003-rich-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/004-surprised-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/005-vomit.png";  big: false }
            ListElement {
                path:"smileys/smiley/006-laughing-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/007-angry-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/008-silent.png";  big: false }
            ListElement {
                path:"smileys/smiley/009-surprised.png";  big: false }
            ListElement {
                path:"smileys/smiley/010-favorite.png";  big: false }
            ListElement {
                path:"smileys/smiley/011-suspicious.png";  big: false }
            ListElement {
                path:"smileys/smiley/012-sick-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/013-liar.png";  big: false }
            ListElement {
                path:"smileys/smiley/014-dribble.png";  big: false }
            ListElement {
                path:"smileys/smiley/015-laughing-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/016-sick-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/017-clown.png";  big: false }
            ListElement {
                path:"smileys/smiley/018-cowboy.png";  big: false }
            ListElement {
                path:"smileys/smiley/019-happy-7.png";  big: false }
            ListElement {
                path:"smileys/smiley/020-robot.png";  big: false }
            ListElement {
                path:"smileys/smiley/021-injured.png";  big: false }
            ListElement {
                path:"smileys/smiley/022-thinking-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/023-nerd.png";  big: false }
            ListElement {
                path:"smileys/smiley/024-sick-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/025-rich.png";  big: false }
            ListElement {
                path:"smileys/smiley/026-secret.png";  big: false }
            ListElement {
                path:"smileys/smiley/027-monkey-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/028-monkey-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/029-monkey.png";  big: false }
            ListElement {
                path:"smileys/smiley/030-thinking.png";  big: false }
            ListElement {
                path:"smileys/smiley/031-happy-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/032-happy-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/033-sad-13.png";  big: false }
            ListElement {
                path:"smileys/smiley/034-cat-8.png";  big: false }
            ListElement {
                path:"smileys/smiley/035-cat-7.png";  big: false }
            ListElement {
                path:"smileys/smiley/036-cat-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/037-cat-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/038-cat-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/039-cat-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/040-cat-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/041-cat-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/042-cat.png";  big: false }
            ListElement {
                path:"smileys/smiley/043-sick.png";  big: false }
            ListElement {
                path:"smileys/smiley/044-muted.png";  big: false }
            ListElement {
                path:"smileys/smiley/045-shocked-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/046-sleeping-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/047-embarrassed-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/048-shocked-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/049-shocked-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/050-embarrassed-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/051-shocked-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/052-shocked-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/053-crying-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/054-shocked-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/055-sad-12.png";  big: false }
            ListElement {
                path:"smileys/smiley/056-sad-11.png";  big: false }
            ListElement {
                path:"smileys/smiley/057-sad-10.png";  big: false }
            ListElement {
                path:"smileys/smiley/058-sad-9.png";  big: false }
            ListElement {
                path:"smileys/smiley/059-sad-8.png";  big: false }
            ListElement {
                path:"smileys/smiley/060-sad-7.png";  big: false }
            ListElement {
                path:"smileys/smiley/061-sad-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/062-angry-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/063-sad-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/064-crying-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/065-angry-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/066-angry-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/067-sad-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/068-sad-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/069-tongue-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/070-tongue-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/071-tongue-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/072-kiss-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/073-kiss-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/074-kiss-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/075-kiss.png";  big: false }
            ListElement {
                path:"smileys/smiley/076-sad-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/077-sad-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/078-sad.png";  big: false }
            ListElement {
                path:"smileys/smiley/079-embarrassed-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/080-angry.png";  big: false }
            ListElement {
                path:"smileys/smiley/081-sleeping.png";  big: false }
            ListElement {
                path:"smileys/smiley/082-shocked.png";  big: false }
            ListElement {
                path:"smileys/smiley/083-smart.png";  big: false }
            ListElement {
                path:"smileys/smiley/084-cool.png";  big: false }
            ListElement {
                path:"smileys/smiley/085-in-love.png";  big: false }
            ListElement {
                path:"smileys/smiley/086-happy-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/087-tongue.png";  big: false }
            ListElement {
                path:"smileys/smiley/088-embarrassed.png";  big: false }
            ListElement {
                path:"smileys/smiley/089-wink.png";  big: false }
            ListElement {
                path:"smileys/smiley/090-devil-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/091-angel.png";  big: false }
            ListElement {
                path:"smileys/smiley/092-laughing-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/093-laughing.png";  big: false }
            ListElement {
                path:"smileys/smiley/094-happy-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/095-happy-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/096-crying.png";  big: false }
            ListElement {
                path:"smileys/smiley/097-happy-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/098-happy.png";  big: false }
            ListElement {
                path:"smileys/smiley/099-poo.png";  big: false }
            ListElement {
                path:"smileys/smiley/100-skull.png";  big: false }
            ListElement {
                path:"smileys/smiley/101-devil.png";  big: false }
            ListElement {
                path:"smileys/smiley/102-space-invaders.png";  big: false }
            ListElement {
                path:"smileys/smiley/103-alien.png";  big: false }
            ListElement {
                path:"smileys/smiley/104-ghost.png";  big: false }
            ListElement {
                path:"smileys/smiley/105-demon-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/106-demon.png";  big: false }
            ListElement {
                path:"smileys/smiley/afro-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/afro.png";  big: false }
            ListElement {
                path:"smileys/smiley/agent.png";  big: false }
            ListElement {
                path:"smileys/smiley/alien-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/alien.png";  big: false }
            ListElement {
                path:"smileys/smiley/angel.png";  big: false }
            ListElement {
                path:"smileys/smiley/angry-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/angry-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/angry-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/angry-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/angry-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/angry.png";  big: false }
            ListElement {
                path:"smileys/smiley/arguing.png";  big: false }
            ListElement {
                path:"smileys/smiley/arrogant.png";  big: false }
            ListElement {
                path:"smileys/smiley/asian-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/asian.png";  big: false }
            ListElement {
                path:"smileys/smiley/avatar.png";  big: false }
            ListElement {
                path:"smileys/smiley/baby-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/baby-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/baby.png";  big: false }
            ListElement {
                path:"smileys/smiley/bully.png";  big: false }
            ListElement {
                path:"smileys/smiley/burglar.png";  big: false }
            ListElement {
                path:"smileys/smiley/businessman.png";  big: false }
            ListElement {
                path:"smileys/smiley/cheeky-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/cheeky.png";  big: false }
            ListElement {
                path:"smileys/smiley/clown.png";  big: false }
            ListElement {
                path:"smileys/smiley/confused-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/confused-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/confused-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/confused.png";  big: false }
            ListElement {
                path:"smileys/smiley/creepy.png";  big: false }
            ListElement {
                path:"smileys/smiley/crying-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/crying-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/crying-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/crying.png";  big: false }
            ListElement {
                path:"smileys/smiley/dazed-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/dazed-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/dazed-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/dazed.png";  big: false }
            ListElement {
                path:"smileys/smiley/dead-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/dead-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/dead-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/dead-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/dead-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/dead-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/dead.png";  big: false }
            ListElement {
                path:"smileys/smiley/desperate-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/desperate.png";  big: false }
            ListElement {
                path:"smileys/smiley/detective.png";  big: false }
            ListElement {
                path:"smileys/smiley/dissapointment.png";  big: false }
            ListElement {
                path:"smileys/smiley/doctor.png";  big: false }
            ListElement {
                path:"smileys/smiley/drunk.png";  big: false }
            ListElement {
                path:"smileys/smiley/dumb.png";  big: false }
            ListElement {
                path:"smileys/smiley/emo-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/emo-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/emo.png";  big: false }
            ListElement {
                path:"smileys/smiley/emoticon.png";  big: false }
            ListElement {
                path:"smileys/smiley/evil.png";  big: false }
            ListElement {
                path:"smileys/smiley/faint-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/faint.png";  big: false }
            ListElement {
                path:"smileys/smiley/flirt-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/flirt-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/flirt.png";  big: false }
            ListElement {
                path:"smileys/smiley/flirty.png";  big: false }
            ListElement {
                path:"smileys/smiley/gangster.png";  big: false }
            ListElement {
                path:"smileys/smiley/geek-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/geek.png";  big: false }
            ListElement {
                path:"smileys/smiley/gentleman-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/gentleman-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/gentleman-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/gentleman-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/gentleman.png";  big: false }
            ListElement {
                path:"smileys/smiley/ginger.png";  big: false }
            ListElement {
                path:"smileys/smiley/girl-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/girl.png";  big: false }
            ListElement {
                path:"smileys/smiley/goofy-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/goofy-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/goofy-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/goofy-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/goofy.png";  big: false }
            ListElement {
                path:"smileys/smiley/grubby-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/grubby.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-10.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-11.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-12.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-13.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-14.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-15.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-16.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-7.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-8.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy-9.png";  big: false }
            ListElement {
                path:"smileys/smiley/happy.png";  big: false }
            ListElement {
                path:"smileys/smiley/harry-potter.png";  big: false }
            ListElement {
                path:"smileys/smiley/heisenberg.png";  big: false }
            ListElement {
                path:"smileys/smiley/hipster-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/hipster-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/hipster.png";  big: false }
            ListElement {
                path:"smileys/smiley/in-love-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/in-love-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/in-love-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/in-love-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/in-love-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/in-love-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/in-love.png";  big: false }
            ListElement {
                path:"smileys/smiley/japan.png";  big: false }
            ListElement {
                path:"smileys/smiley/jew.png";  big: false }
            ListElement {
                path:"smileys/smiley/joyful-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/joyful-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/joyful.png";  big: false }
            ListElement {
                path:"smileys/smiley/kiss-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/kiss-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/kiss-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/kiss-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/kiss.png";  big: false }
            ListElement {
                path:"smileys/smiley/laughing-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/laughing-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/laughing-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/laughing.png";  big: false }
            ListElement {
                path:"smileys/smiley/listening.png";  big: false }
            ListElement {
                path:"smileys/smiley/love.png";  big: false }
            ListElement {
                path:"smileys/smiley/manly.png";  big: false }
            ListElement {
                path:"smileys/smiley/miserly-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/miserly.png";  big: false }
            ListElement {
                path:"smileys/smiley/nerd-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/nerd-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/nerd-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/nerd-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/nerd.png";  big: false }
            ListElement {
                path:"smileys/smiley/ninja.png";  big: false }
            ListElement {
                path:"smileys/smiley/pirate-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/pirate-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/pirate.png";  big: false }
            ListElement {
                path:"smileys/smiley/punk-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/punk-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/punk.png";  big: false }
            ListElement {
                path:"smileys/smiley/rapper.png";  big: false }
            ListElement {
                path:"smileys/smiley/relieved.png";  big: false }
            ListElement {
                path:"smileys/smiley/rich-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/rich-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/rich.png";  big: false }
            ListElement {
                path:"smileys/smiley/rockstar.png";  big: false }
            ListElement {
                path:"smileys/smiley/sad-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/sad-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/sad-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/sad-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/sad-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/sad-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/sad.png";  big: false }
            ListElement {
                path:"smileys/smiley/scared-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/scared-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/scared-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/scared.png";  big: false }
            ListElement {
                path:"smileys/smiley/sceptic-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/sceptic-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/sceptic-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/sceptic-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/sceptic-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/sceptic-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/sceptic-7.png";  big: false }
            ListElement {
                path:"smileys/smiley/sceptic.png";  big: false }
            ListElement {
                path:"smileys/smiley/secret.png";  big: false }
            ListElement {
                path:"smileys/smiley/shocked-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/shocked-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/shocked-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/shocked.png";  big: false }
            ListElement {
                path:"smileys/smiley/sick-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/sick-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/sick-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/sick-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/sick.png";  big: false }
            ListElement {
                path:"smileys/smiley/silent.png";  big: false }
            ListElement {
                path:"smileys/smiley/skeleton.png";  big: false }
            ListElement {
                path:"smileys/smiley/smile.png";  big: false }
            ListElement {
                path:"smileys/smiley/smiling-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/smiling.png";  big: false }
            ListElement {
                path:"smileys/smiley/smoked.png";  big: false }
            ListElement {
                path:"smileys/smiley/smug-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/smug-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/smug-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/smug-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/smug-5.png";  big: false }
            ListElement {
                path:"smileys/smiley/smug-6.png";  big: false }
            ListElement {
                path:"smileys/smiley/smug.png";  big: false }
            ListElement {
                path:"smileys/smiley/sporty.png";  big: false }
            ListElement {
                path:"smileys/smiley/stunned.png";  big: false }
            ListElement {
                path:"smileys/smiley/superhero-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/superhero-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/superhero-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/superhero-4.png";  big: false }
            ListElement {
                path:"smileys/smiley/superhero.png";  big: false }
            ListElement {
                path:"smileys/smiley/surprised-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/surprised.png";  big: false }
            ListElement {
                path:"smileys/smiley/thinking.png";  big: false }
            ListElement {
                path:"smileys/smiley/tired-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/tired-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/tired-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/tired.png";  big: false }
            ListElement {
                path:"smileys/smiley/tough-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/tough.png";  big: false }
            ListElement {
                path:"smileys/smiley/trendy.png";  big: false }
            ListElement {
                path:"smileys/smiley/vampire-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/vampire.png";  big: false }
            ListElement {
                path:"smileys/smiley/wink-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/wink-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/wink.png";  big: false }
            ListElement {
                path:"smileys/smiley/winking-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/winking.png";  big: false }
            ListElement {
                path:"smileys/smiley/yawning-1.png";  big: false }
            ListElement {
                path:"smileys/smiley/yawning-2.png";  big: false }
            ListElement {
                path:"smileys/smiley/yawning-3.png";  big: false }
            ListElement {
                path:"smileys/smiley/yawning.png";  big: false }
            ListElement {
                path:"smileys/smiley/yelling.png";  big: false }
            ListElement {
                path:"smileys/smiley/zombie.png";  big: false }

            */
        }
    }

    Rectangle {
        id: linkDialog
        height: parent.height * .9
        width: parent.width * .9
        anchors.centerIn: parent
        color: "grey"
        visible: false

        signal linkToAppend( var link )

        Label {
            id: linkLabel
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Link:"
            color: "white"
        }

        TextField {
            id: linkTextField
            anchors.top: linkLabel.bottom
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * .75
            clip: true
        }

        Label {
            id: displayTextLabel
            anchors.top: linkTextField.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 5
            text: "Diplay text:"
            color: "white"
        }

        TextField {
            id: linkDisplayTextField
            anchors.top: displayTextLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 5
            width: parent.width * .75
            clip: true
        }

        Button {
            id: cancelPushButton
            anchors.left: linkDisplayTextField.left
            anchors.top: linkDisplayTextField.bottom
            anchors.topMargin: 10
            width: linkDisplayTextField.width / 2 - 5
            text: "Cancel"
            onClicked: {
                linkTextField.text = ""
                linkDisplayTextField.text = ""
                linkDialog.visible = false
            }
        }

        Button {
            id: acceptPushButton
            anchors.right: linkDisplayTextField.right
            anchors.top: linkDisplayTextField.bottom
            anchors.topMargin: 10
            width: linkDisplayTextField.width / 2 - 5
            text: "Accept"
            onClicked: {
                var textToAppend = "<a href=\"" + linkTextField.text + "\">" + linkDisplayTextField.text + "</a>"
                linkTextField.text = ""
                linkDisplayTextField.text = ""
                linkDialog.visible = false
                linkDialog.linkToAppend( textToAppend )
            }
        }
    }
}
